# Audio Recorder

Android friendly!

Audio Recorder with custom recording folder, nice recording volume indicator, recording notification, recording lock screen activity.

# Manual installation

    gradle installDebug

# Screenshots

![shot](/docs/shot.png)

# Contributions

  * please open an issue or file a merge request
  * merge documentation against `master` branch

## Translation

If you want to translate 'Audio Recorder' to your language  please read this:

  * [HOWTO-Translate.md](/docs/HOWTO-Translate.md)

## Contributors

  * Japanese translation thanks to @naofumi
  * German translation thanks to @vv01f
